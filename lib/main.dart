import 'package:kh_mob_dev_dart_presentation/asynchrony.dart';
import 'package:kh_mob_dev_dart_presentation/functions_and_classes.dart';
import 'package:kh_mob_dev_dart_presentation/variables_and_types.dart';


/// REPOSITORY LINK https://bitbucket.org/EgorLogachev/dartkhmobdevs
void main() {

  variablesAndTypes();
  functions();
  asynchrony();

}