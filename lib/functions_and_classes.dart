import 'package:flutter/foundation.dart';


/// REPOSITORY LINK https://bitbucket.org/EgorLogachev/dartkhmobdevs
void functions() {

  /// Functions definition. https://dart.dev/guides/language/language-tour#functions
  bool isValid(String password) {
    return password != null && password.isNotEmpty && password.length > 3;
  }

  isValid2(String password) {
    return password != null && password.isNotEmpty && password.length > 3;
  }

  bool isValid3(String password) => password != null && password.isNotEmpty && password.length > 3;



  /// Optional params https://dart.dev/guides/language/language-tour#optional-parameters
  /// Named parameters
  void optionalParams({bool first, @required String second}) {
    print('Optional params - param1: $first, param2: $second');
  }

  ///WARNING param1 required
  optionalParams(first: false);
  optionalParams(second: 'str');
  optionalParams(first: true, second: 'second');
  optionalParams(second: 'asdf', first: false);

  void defaultOptionalParams({int first = 0, bool second = false}) {
    print('Default optional params - param1: $first, param2: $second');
  }

  defaultOptionalParams();



  /// Positional params
  void positionalParams([int first, int second]) {
    print('Positional params - param1: $first, param2: $second');
  }

  positionalParams(0);
  positionalParams(0, 1);

  void defaultPositionalParams([String first = 'first', String second = 'second']) {
    print('Default positional params - param1: $first, param2: $second');
  }

  defaultPositionalParams();



  /// Anonymous functions, Lambdas and First class object
  /// https://dart.dev/guides/language/language-tour#functions-as-first-class-objects
  /// https://dart.dev/guides/language/language-tour#anonymous-functions
  ///   ([[Type] param1[, …]]) {
  ///     codeBlock;
  ///   };
  var list = [0, 1, 2];
  list.forEach((item) {
    print(item);
  });

  list.forEach((item) => print(item));

  /// Define function as variable
  var printVar = (item) => print(item);
  void Function(int) printFunc = printVar;

  list.forEach(printFunc);

  void printItem(int item) {
    print(item);
  }

  list.forEach(printItem);



  /// Type test https://dart.dev/guides/language/language-tour#type-test-operators
  num numValue = 10;
  if (numValue is int) numValue.toInt();

  if (numValue is! int) numValue.toDouble();

  (numValue as int).isOdd;



  /// todo ...? https://dart.dev/guides/language/language-tour#spread-operator
  /// Null check operations and operators
  /// Conditional assignment. https://dart.dev/guides/language/language-tour#assignment-operators
  var lazy;
  lazy ??= 10;
  print("First result of '??=' $lazy");

  lazy ??= 20;
  print("Second result of '??=' $lazy");



  /// Conditional null check expression https://dart.dev/guides/language/language-tour#conditional-expressions
  String nullCheckExpression(String value) => value ?? 'value is null';

  print(nullCheckExpression(null));
  print(nullCheckExpression('not null'));



  /// Conditional member access
  var nullValue;
  nullValue?.toString();



  /// Spread operator with null check
  var innerList;
  var spreadList = [0, ...?innerList];
  print('Spread operator result: $spreadList');



  /// Cascade https://dart.dev/guides/language/language-tour#cascade-notation-
  StringBuffer stringBuffer = StringBuffer()
    ..write('fezz')
    ..write('bazz')
    ..length; /// WARNING!!! Cascade ignore any subsequent values that might be returned



  /// will be used later
  testConstConstructor();
  filterOdds();
}



/// Classes. https://dart.dev/guides/language/language-tour#classes
/// Constructors https://dart.dev/guides/language/language-tour#constructors
class SampleClass {
  String _field1;
  int field2;

//  SampleClass(String field1, int field2) {
//    this._field1 = field1;
//    this.field2 = field2;
//  }

  SampleClass(this._field1, this.field2);

  /// Named constructor.
  SampleClass.origin() {
    _field1 = '';
    field2 = 0;
  }

  /// Private constructor.
  SampleClass._private() {
    _field1 = '';
    field2 = 0;
  }
}



/// Object creation order:
/// 1. initializer list
/// 2. superclass’s no-arg constructor
/// 3. main class’s no-arg constructor
class Rect {
  final int left;
  final int right;
  final int top;
  final int bottom;

  Rect(this.left, this.top, this.right, this.bottom);

  /// initializer list
  Rect.copy(Rect rect)
      : left = rect.left,
        top = rect.top,
        right = rect.right,
        bottom = rect.bottom;

  Rect.clone(Rect rect) : this(rect.left, rect.top, rect.right, rect.bottom);
}



/// Constant constructors
class Circle {
  final int radius;

  const Circle(this.radius);
}

void testConstConstructor() {
  const radius = 10;
  var constCircle1 = const Circle(radius);
  var constCircle2 = const Circle(radius);
  var circle = Circle(radius);
  print('Values are identical ${identical(constCircle1, constCircle2)}');
  print('Values are identical ${identical(constCircle1, circle)}');
}



/// Factory constructors
class Shape {
  var someValue;

  Shape();

  factory Shape.createShape() {
    /// WARNING!!! Have no access to this.
//    this.someValue = 10;
    return Shape();
  }

  /// Factory constructors can create instances of inheritors
//  factory Shape.createRect(int left, int right, int top, int bottom) => Rect(left, top, right, bottom);
}



/// Getters and setters. https://dart.dev/guides/language/language-tour#methods
class DateFormatter {
  DateTime dateTime;

  DateFormatter(this.dateTime);

  get dateAsString => dateTime.toString();

  set dateAsString(String date) => this.dateTime = DateTime.parse(date);
}



/// Interfaces. https://dart.dev/guides/language/language-tour#implicit-interfaces
/// Dart have no interfaces, but...
class Transport {
  void deliver() {
    // todo some computations
  }
}

class Truck extends Transport {
  @override
  void deliver() {
    super.deliver();
    // todo some computations
  }
}

class Ship implements Transport {
  @override
  void deliver() {
    // todo some computations
  }
}



/// Overriding operators. https://dart.dev/guides/language/language-tour#extending-a-class
class User {
  final String firstName;
  final String secondName;

  User(this.firstName, this.secondName);

  @override
  bool operator ==(other) {
    return other != null && other is User && other.firstName == firstName && other.secondName == secondName;
  }
}



/// Enums. https://dart.dev/guides/language/language-tour#enumerated-types
enum Compass { north, south, east, west }



/// Mixin. https://dart.dev/guides/language/language-tour#adding-features-to-a-class-mixins
class SimpleScreen {}

class SpecialScreen {
  void specialMethod() {}
}

class FirstScreen extends SimpleScreen with Popup {
  @override
  void showPopup() {
    super.showPopup();
  }
}

class SecondScreen extends SpecialScreen with Popup, Dialog {
  @override
  void showPopup() {
    super.showPopup();
  }

  @override
  void showDialog() {
    super.showDialog();
  }
}

mixin Popup {
  void showPopup() {}
}

mixin Dialog on SpecialScreen {
  void showDialog() {
    specialMethod();
  }
}



/// Typedefs. https://dart.dev/guides/language/language-tour#typedefs
typedef Predicate<T> = bool Function(T);

List<int> filter(List<int> list, Predicate<int> predicate) {
  return list.where(predicate);
}

void filterOdds() {
  var filtered = filter([1,2,3,4], (item) => item.isOdd);
  print(filtered);
}
