/// REPOSITORY LINK https://bitbucket.org/EgorLogachev/dartkhmobdevs

import 'dart:async';
import 'dart:core';


/// REPOSITORY LINK https://bitbucket.org/EgorLogachev/dartkhmobdevs
void asynchrony() {

  /// Futures.
  /// https://dart.dev/guides/language/language-tour#handling-futures
  /// https://dart.dev/guides/libraries/library-tour#future
  /// https://api.dart.dev/stable/2.6.1/dart-async/Future-class.html
  /// https://dart.dev/codelabs/async-await

  void futureExample1() {
    print('Start');
    Future(() {
      print('Return initial value');
      return 2;
    }).then((result) {
      print('Calculation');
      return result + 2;
    }).then(print);
    print('End');
  }

  /// Async-await future sugar syntax
  Future<void> futureExample2() async {

    Future<int> initialValue() async {
      print('Return initial value');
      return 2;
    }

    Future<int> calculation(int initial) async {
      print('Calculation');
      return initial + 2;
    }

    void printResult(int result) {
      print(result);
    }

    print('Start');
    Future(initialValue)
        .then(calculation)
        .then(printResult);
    print('End');

    print('Start sugar');
    final initial = await initialValue();
    final result = await calculation(initial);
    printResult(result);
    print('End sugar');
  }

  /// Future wait.
  futureWaitExample() async {

    Future<int> getTwo() async => 2;
    Future<int> getThree() async => 3;
    Future<int> getFour() async => 4;

    Future.wait([
      getTwo(), 
      getThree(), 
      getFour()
    ]).then((results) {
      var sum = 0;
      results.forEach((result) {
        sum += result;
      });
      print('Future wait result: $sum');
    });

    print('Future wait sugar result: ${await getTwo() + await getThree() + await getFour()}');
  }
  futureExample1();
  futureExample2();
  futureWaitExample();
  
  
  
  /// Streams.
  /// https://dart.dev/guides/language/language-tour#handling-streams
  /// https://dart.dev/guides/libraries/library-tour#stream
  /// https://api.dart.dev/stable/2.6.1/dart-async/Stream-class.html
  /// https://dart.dev/guides/language/language-tour#generators

  /// Create stream.

  final valueStream = Stream.value(1);

  final listStream = Stream.fromIterable([1, 2, 3]);

  StreamController streamController = StreamController<int>();
  final controllerStream = streamController.stream;
  for (var i = 0; i < 10; i++) {
    streamController.add(i);
  }
  /// WARNING!!! StreamController should be closed.

  Stream<int> generateStream(int n) async* {
    for (var i = 0; i < n; i++) {
      yield i;
    }
  }

  final generatedStream = generateStream(10);




  /// Offtop. Iterable object generation.
  Iterable<int> generateIterable(int n) sync* {
    for (var i = 0; i < n; i++) {
      yield i;
    }
  }

  final generatedIterable = generateIterable(20);




  /// Listen stream.
  var streamSubscription = streamController.stream.listen((value) {
    print(value);
  });

  streamSubscription.resume();
  streamSubscription.pause();
  streamSubscription.cancel();

  streamController.close();

  Future<void> testAwaitFor () async {
    await for (var value in generatedStream) {
      print(value);
    }
  }

  testAwaitFor();
}




