
/// REPOSITORY LINK https://bitbucket.org/EgorLogachev/dartkhmobdevs
void variablesAndTypes() {
  /// Variables. https://dart.dev/guides/language/language-tour#variables
  /// strong dynamic type
  var variable = 0;
  /// WARNING!!!
//  variable = 1.2;

  /// unspecified type
  dynamic anyType = 12.3;
  anyType = 23;

  /// specific type
  int integer = 1;

  /// When should we use dynamic or static type definition?
  /// https://dart.dev/guides/language/effective-dart/design#types



  /// Default value null. https://dart.dev/guides/language/language-tour#default-value
  int otherInt;
  int nullableInt = null;
  print('Print default value $otherInt');
  print('Print nullable value $nullableInt');



  /// Final and const. https://dart.dev/guides/language/language-tour#final-and-const
  /// Define Final.
  final e = 2.718;
  final double eValue = 2.718;
  /// IMPORTANT A final variable can be set only once.

  /// Define Const.
  const pi = 3.14;
  const double doublePi = 2 * pi;
//  const doubleE = 2 * eValue;
  /// IMPORTANT A const variable is a compile-time constant.



  /// Build in types https://dart.dev/guides/language/language-tour#built-in-types
  /// Numbers.
  /// int 64-bit
  int integerValue = 10;
  int hexValue = 0xabcdef;

  /// double 64-bit
  double doubleValue = 4.67;

  /// num.
  /// int, double are subtypes of num.
  num intNum = integerValue;
  print('Integer num: $intNum');

  num doubleNum = doubleValue;
  print('Double num: $doubleNum');



  /// Strings. https://dart.dev/guides/language/language-tour#strings
  var singleQuotes = 'Simple string with single quotes';
  var doubleQuotes = "Double quotes if you wanna use ' in text without escaping";

  const concat =  'few' 'strings' 'concatenation';
  print('Concatenated string: $concat');

  var value = 'concatenation';
  var otherConcat = value + 'of' + 'few'
      "strings";
  print('One more concatenated string $otherConcat');



  /// Runes. UTF-32 code points of a string. https://dart.dev/guides/language/language-tour#runes
  print('\u{1f60e}');
  /// More details: https://dart.dev/guides/language/language-tour#runes



  /// Collections.
  /// Lists. https://dart.dev/guides/language/language-tour#lists
  var emptyList = [];
  final list = ['array', 'of', 'words'];
  print('Default list: $list');
  list[0] = 'list';
  print('Modified list: $list');
  /// WARNING!!!
//  list[1] = 0;

  final numList = [1, 2.0, 3.14];
  numList[0] = 20;
  numList[1] = 23.4;
  /// WARNING!!!
//  numList[3] = 'dasf';

  final strongIntList = <int>[0, 1, 2, 3];
  strongIntList[0] = 20;
  /// WARNING!!!
//  strongIntList[2] = 'asdfds';

  var constantList = const [1, 2, 3];
  /// WARNING!!! IDE allow you to do this, but this code will throw error in result
//  constantList[1] = 1;



  /// Sets. https://dart.dev/guides/language/language-tour#sets
  var emptySet = {};
  var set = {'Collection', 'of', 'unique', 'elements'};



  ///Maps. https://dart.dev/guides/language/language-tour#maps
  var map = {
    // Key:    Value
    'first' : 0,
    'second': 1,
    'third': 2
  };
  map['fourth'] = 3;

  for (final key in map.keys) {
      print('Print map value: ${map[key]} with key: $key');
  }



  /// Collections features from Dart version 2.3
  /// Operator IF in collections.
  /// https://github.com/dart-lang/language/blob/master/accepted/2.3/control-flow-collections/feature-specification.md
  var isAdd = true;
  var logicalGeneratedList = <int>[
    1,
    2,
    if (isAdd) 4,
    isAdd ? 5 : 6,
    if (isAdd)
      7
    else if (!isAdd)
      8
    else
      9
  ];
  print('Collection with IF: $logicalGeneratedList');



  /// Operator FOR in collections.
  var loopGeneratedList = [
    for (int i = 0; i < 10; i++) 'Index: $i'
  ];
  print('Collection with FOR: $loopGeneratedList');



  /// Spread operator.
  /// https://github.com/dart-lang/language/blob/master/accepted/2.3/spread-collections/feature-specification.md
  var innerList = [1, 2, 3];
  var spreadList = [0, ...innerList];
  print('Spread operator result: $spreadList');
}
